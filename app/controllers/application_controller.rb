class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # before_action :configure_permitted_parametes, if: :devise_controller?

  # def after_sign_in_path_for(resource_or_scope)
  #   if resource.is_a?(Admin)
  #     authenticated_admin_root_path
  #   elsif resource.is_a?(Cashier)
  #     authenticated_cashier_root_path
  #   end
  # end

  # protected
  #   def configure_permitted_parameters
  #     devise_parameter_sanitizer_permit(:sign_up, keys: [:name])
  #   end
end
