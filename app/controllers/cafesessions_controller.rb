class CafesessionsController < ApplicationController
  before_action :set_cafesession, only: [:edit, :show, :update, :destroy, :add_order, :add_service, :customer_summary, :final_receipt]
  before_action :authenticate_cashier!
  
  def index
    @cafesessions = Cafesession.where("finished = false").order('time_in')
  end

  def customer_summary
    start = @cafesession.time_in
    finish = DateTime.now
    
    # Saving the shit
    @cafesession.time_out = finish
    @cafesession.save
    
    duration = @cafesession.time_out.to_time - start.to_time

    hours = duration/60/60
    minutes = duration/60%60
    seconds = duration%60

    @duration_in_store = "#{hours.to_i}h #{minutes.to_i}min #{seconds.to_i}sec"

    # every hour 70, after 5 hours 350
    # 0:00:00-1:15:59, 70
    # 1:16:00-2:15:59, 140
    # 2:16:00-3:15:59, 210
    # 3:16:00-4:15:59, 280
    # 4:16:00-forever, 350

    # I wish to optimize this messy code lol
    first_hour = (1 * 3600 + 16 * 60)
    second_hour = (2 * 3600 + 16 * 60)
    third_hour = (3 * 3600 + 16 * 60)
    fourth_hour = (4 * 3600 + 16 * 60)

    fee = 0
    hour_bracket = 0

    if duration < first_hour
      fee = 70
      hour_bracket = 1
    elsif duration >= first_hour and duration < second_hour
      fee = 70 * 2
      hour_bracket = 2
    elsif duration >= second_hour and duration < third_hour
      fee = 70 * 3
      hour_bracket = 3
    elsif duration >= third_hour and duration < fourth_hour
      fee = 70 * 4
      hour_bracket = 4
    elsif duration >= fourth_hour
      fee = 70 * 5
      hour_bracket = 5
    end

    @stayfee = fee
    @hour_bracket = hour_bracket
  end

  def final_receipt
    @cafesession.finished = true
    @customer = @cafesession.customer
    @customer.in_store = false

    if @cafesession.save and @customer.save
      flash[:notice] = "Goodbye! #{@customer.customer_name}"
    else
      flash[:alert] = "Error"
    end
  end

  def create
    @customer = Customer.find_by_id(params[:customer_id])
    if @customer.in_store
      redirect_to customers_path, alert: "That customer is already timed in."
    else
      @cafesession = current_cashier.cafesessions.new(customer: @customer)
      @cafesession.time_in = DateTime.now
      @cafesession.time_out = DateTime.now
      @cafesession.subtotal = 0
      @cafesession.date = Date.today
      @cafesession.table_number = 0
      @cafesession.discount_code = DiscountCode.find('NoDiscount')
      @cafesession.card_number = nil
      @cafesession.discount_amount = 0
      @cafesession.total_amount = 0
      @cafesession.vat = 0
      @cafesession.cash = 0
      @cafesession.finished = false
  
      @customer.in_store = true
      
      if @cafesession.save and @customer.save
        redirect_to cafesessions_path, notice: "Customer Successfully Timed in."
      else
        redirect_to customers_path, alert: "Customer Couldn't be Timed in."
      end
    end
  end

  def update
    if params[:commit] == 'Update table number'
      if @cafesession.update(cafesession_params)
        redirect_to @cafesession, notice: "Table number updated."
      else
        redirect_to @cafesession, alert: "Invalid table number"
      end
    elsif params[:commit] == 'Save orders' || params[:commit] == 'Save services'
      if params[:commit] == 'Save orders'
        # checks if order is too much
        # @cafesession.order_lines.each do |order_line|
        #   order_too_much = order_line.order_quantity <
        # end

        if @cafesession.update(cafesession_params_add_order)
          redirect_to @cafesession, notice: "Order Successfully Sent."
        else
          redirect_to add_order_cafesession_path(@cafesession), alert: "Order Not Sent."
        end
      elsif params[:commit] == 'Save services'
        if @cafesession.update(cafesession_params_add_service)
          redirect_to @cafesession, notice: "Service Successfully Added."
        else
          redirect_to add_service_cafesession_path(@cafesession), alert: "Service Not Added."
        end
      end
    end
    if params[:commit] == 'Proceed to Final Receipt'
      if @cafesession.update(cafesession_params)
        # redirect_to final_receipt_cafesessions_path(@cafesession)"
      else
        flash[:alert] = "Error"
      end
    end
  end

  def change_status #changes the status of the ORDER
    @order_line = OrderLine.find_by(id: params[:id])
    @order_item = @order_line.order_item

    if @order_line.order_status == false
      @order_line.order_status = true
      temp = @order_item.item_product_level
      @order_item.item_product_level -= @order_line.order_quantity

      if @order_line.save and @order_item.save
        redirect_to pending_orders_cafesessions_path, notice: "Order served.\nProduct level of #{@order_line.order_item.item_name}: #{temp} - #{@order_line.order_quantity} = #{@order_item.item_product_level}"
      else
        redirect_to pending_orders_cafesessions_path, alert: "Error."
      end
    else
      redirect_to pending_orders_cafesessions_path, alert: "Error."
    end
  end

  private
    def set_cafesession
      @cafesession = Cafesession.find_by(id: params[:id])
      redirect_to cafesessions_path, alert: "Cafe Session Not Found." if @cafesession.nil?
    end
    
    def cafesession_params_add_order
      params.require(:cafesession).permit(order_lines_attributes: [:order_item_id, :cafesession_id, :order_status, :order_quantity, :_destroy])
      # OrderLine.attribute_names.map(&:to_sym).push(:_destroy)
    end

    def cafesession_params_add_service
      params.require(:cafesession).permit(service_lines_attributes: [:service_id, :cafesession_id, :service_quantity, :_destroy])
      # ServiceLine.attribute_names.map(&:to_sym).push(:_destroy)
    end

    def cafesession_params
      params.require(:cafesession).permit!
    end
end