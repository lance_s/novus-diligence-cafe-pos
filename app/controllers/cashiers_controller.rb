class CashiersController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :set_cashier, only: [:edit, :show, :update, :destroy]
  before_action :authenticate_admin!
  
  def index
    @cashiers = Cashier.order(sort_column + " " + sort_direction)
  end

  def new
    @cashier = current_admin.cashiers.build
  end
  
  def create
   @cashier = current_admin.cashiers.new(cashier_params)
    if @cashier.save
      redirect_to cashiers_path, notice: "Cashier successfully added."
    else
      render :new
    end
  end
  
  def edit
    @cashier = Cashier.find(params[:id])
  end

  def update
    @cashier = Cashier.find(params[:id])
    if @cashier.update_attributes(cashier_params)
      redirect_to cashiers_path, notice: "Cashier successfully updated."
    else
      render :edit
    end
  end

  def show
  end
 
  # def destroy #not using kay wala may status
  #   @cashier = Cashier.find(params[:id])
  #   if @cashier.status == true
  #     @cashier.status = false
  #   else
  #     @cashier.status = true
  #   end
    
  #   if @cashier.save
  #     redirect_to cashiers_path, notice: "Cashier successfully updated."
  #   end
  # end
  
  #version below turns change status to "delete cashier"
  # def destroy
  #   @cashier = Cashier.find(params[:id])
  #   @cashier.destroy
    
  #   redirect_to cashiers_path
  # end
  # def update # unya na ni
  #   @cashier = Cashier.find(params[:id])
  #   @cashier.destroy
  #   @cashier = current_admin.cashiers.new(cashier_params)
  #   if @cashier.save
  #     redirect_to cashiers_path, notice: "Cashier successfully updated."
  #   else
  #     render :edit
  #   end
  # end
  
  private
    def set_cashier
      @cashier = Cashier.find_by(id: params[:id])
      redirect_to cashiers_path, notice: "Cashier not found." if @cashier.nil?
    end
    
    def cashier_params
       params.require(:cashier).permit(:email, :password, :password_confirmation, :name, :address, :mobile_number, :date_hired) #-> Tried this
    end
    
    def sort_column
      Cashier.column_names.include?(params[:sort]) ? params[:sort] : "name"
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end
end