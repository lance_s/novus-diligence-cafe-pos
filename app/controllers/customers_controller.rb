class CustomersController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :set_customer, only: [:edit, :show, :update, :destroy]
  before_action :authenticate_cashier!
  
  def index
    @customers = Customer.order(sort_column + " " + sort_direction)
    # @customers = Customer.all
  end

  def new
    @customer = Customer.new
  end
  
  def create
    @customer = Customer.new(customer_params)
    @customer.membership = false
    @customer.in_store = false
    
    if @customer.save
      redirect_to customers_path, notice: "Customer successfully added."
    else
      render :new
    end
  end
  
  def edit
    @customer = Customer.find(params[:id])
  end

  def update
    if @customer.update_attributes(customer_params)
      redirect_to customers_path, notice: "Customer successfully updated."
    else
      render :edit
    end
  end

  # def destroy
  #   @customer = Customer.find(params[:id])
  #   @customer.destroy
  
  #   redirect_to customers_path
  # end
  
  private
    def set_customer
      @customer = Customer.find_by(id: params[:id])
      redirect_to customers_path, notice: "Customer not found." if @customer.nil?
    end
    
    def customer_params
      params.require(:customer).permit(:customer_name, :school_organization, :age, :mobile_number, :membership)
    end

    def sort_column
      Customer.column_names.include?(params[:sort]) ? params[:sort] : "customer_name"
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end
end
