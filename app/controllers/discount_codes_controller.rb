class DiscountCodesController < ApplicationController
  before_action :set_discount_code, only: [:edit, :show, :update, :destroy]
  before_action :authenticate_admin!

  def index
    @discount_codes = DiscountCode.all
  end

  def new
    @discount_code = DiscountCode.new
  end
  
  def create
    @discount_code = DiscountCode.new(discount_code_params)
    
    if @discount_code.save
      redirect_to discount_codes_path, notice: "Discount code successfully added."
    else
      render :new
    end
  end

  def edit
    @discount_code = DiscountCode.find(params[:id])
  end

  def update
    @discount_code = DiscountCode.find(params[:id])
    if @discount_code.update_attributes(discount_code_params)
      redirect_to discount_codes_path, notice: "Discount code successfully updated."
    else
      render :edit
    end
  end

  private
    def set_discount_code
      @discount_code = DiscountCode.find_by(id: params[:id])
      redirect_to discount_codes_path, notice: "Discount codes not found." if @discount_code.nil?
    end
    
    def discount_code_params
      params.require(:discount_code).permit(:discount_code, :percentage, :code_status)
    end
end
