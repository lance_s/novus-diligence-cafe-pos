class OrderItemsController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :set_order_item, only: [:edit, :show, :update, :destroy]
  before_action :authenticate!

  def index
    @order_items = OrderItem.order(sort_column + " " + sort_direction)
  end

  def undersupplied_products
    @order_items_u = OrderItem.where("item_product_level <= 5")
    @order_items = OrderItem.order(sort_column + " " + sort_direction)
  end

  def new
    @order_item = OrderItem.new
  end
  
  def create
    @order_item = OrderItem.new(order_item_params)
    
    if @order_item.save
      redirect_to order_items_path, notice: "Product successfully added."
    else
      render :new
    end
  end

  def update
    if @order_item.update_attributes(order_item_params)
      redirect_to order_items_path, notice: "Order item successfully updated."
    else
      render :edit
    end
  end

  private
    def set_order_item
      @order_item = OrderItem.find_by(id: params[:id])
      redirect_to order_items_path, notice: "Product not found." if @order_item.nil?
    end
    
    def order_item_params
      params.require(:order_item).permit(:item_name, :item_type, :item_price, :item_product_level, :item_status)
    end

    def sort_column
      OrderItem.column_names.include?(params[:sort]) ? params[:sort] : "item_name"
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end
    
    def authenticate!
      puts params.inspect
      if cashier_signed_in?
        if params[:action] == 'undersupplied_products'
          authenticate_cashier!
        else
          authenticate_admin!
        end
      elsif admin_signed_in?
        authenticate_admin!
      else
        if params[:action] == 'undersupplied_products'
          authenticate_cashier!
        else
          authenticate_admin!
        end
      end
    end
end
