class ServicesController < ApplicationController
  before_action :set_services, only: [:edit, :show, :update, :destroy]
  before_action :authenticate_admin!

  def index
    @services = Service.all
  end

  def new
    @service = Service.new
  end
  
  def create
    @service = Service.new(service_params)
    
    if @service.save
      redirect_to services_path, notice: "Service successfully added."
    else
      render :new
    end
  end

  def edit
    @service = Services.find(params[:id])
  end

  def update
    @service = Services.find(params[:id])
    if @service.update_attributes(service_params)
      redirect_to services_path, notice: "Service successfully updated."
    else
      render :edit
    end
  end

  private
    def set_service
      @service = Service.find_by(id: params[:id])
      redirect_to services_path, notice: "Service not found." if @service.nil?
    end
    
    def service_params
      params.require(:service).permit(:service_name, :service_price, :service_status)
    end
end
