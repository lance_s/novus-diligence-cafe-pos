class Cafesession < ApplicationRecord
    validates :table_number, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }
    has_many :order_lines, inverse_of: :cafesession
    has_many :order_items, through: :order_lines
    has_many :service_lines, inverse_of: :cafesession
    has_many :services, through: :service_lines
    accepts_nested_attributes_for :order_lines, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :service_lines, reject_if: :all_blank, allow_destroy: true
    # accepts_nested_attributes_for :order_items
    belongs_to :cashier
    belongs_to :customer
    belongs_to :discount_code
end
