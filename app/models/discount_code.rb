class DiscountCode < ApplicationRecord
    validates :discount_code, uniqueness: {message: "already exists.", case_sensitive: false}
    has_many :cafesessions
end
