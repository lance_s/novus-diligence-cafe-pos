class OrderItem < ApplicationRecord
    validates :item_name, uniqueness: {message: "already exists.", case_sensitive: false}
    has_many :order_lines, inverse_of: :order_item
    has_many :cafesessions, through: :order_lines
end
