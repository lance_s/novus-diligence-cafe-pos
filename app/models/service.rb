class Service < ApplicationRecord
    validates :service_name, uniqueness: {message: "already exists.", case_sensitive: false}
    has_many :service_lines, inverse_of: :service
    has_many :cafesessions, :through => :service_lines
end
