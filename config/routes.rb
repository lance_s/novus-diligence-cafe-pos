Rails.application.routes.draw do
  
  devise_for :admins
  devise_scope :admin do
    get 'admins/sign_in', to: 'home#index' #failed sign ins will redirect to main page
    authenticated :admin do
      root 'order_items#index', as: :authenticated_admin_root
    end
  end
  
  devise_for :cashiers
  devise_scope :cashier do
    get 'customers/sign_in', to: 'home#index' #failed sign ins will redirect to main page
    authenticated :cashier do
      root 'cafesessions#index', as: :authenticated_cashier_root
    end
  end
  
  # wala nako gibutang ang mga resources sa devise scope because naa ra man sa controller ang before_authenticate
  
  resources :discount_codes
  
  resources :cashiers
  
  resources :services
  
  resources :order_items do
    collection do
      get 'undersupplied_products', to: 'order_items#undersupplied_products', as: 'undersupplied_products'
    end
  end
  
  resources :customers
  
  resources :cafesessions do
    collection do
      get ':id/customer_summary', to: 'cafesessions#customer_summary', as: 'customer_summary'
      get ':id/add_order', to: 'cafesessions#add_order', as: 'add_order'
      get ':id/add_service', to: 'cafesessions#add_service', as: 'add_service'
      get 'pending_orders', to: 'cafesessions#pending_orders', as: 'pending_orders'
      get 'change_status', to: 'cafesessions#change_status' #gets function and loads it
      get 'final_receipt', to: 'cafesessions#final_receipt'
    end
  end
  
  root 'home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
