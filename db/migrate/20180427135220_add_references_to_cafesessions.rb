class AddReferencesToCafesessions < ActiveRecord::Migration[5.1]
  def change
    add_reference :cafesessions, :cashier, foreign_key: true
    add_reference :cafesessions, :customer, foreign_key: true
  end
end
