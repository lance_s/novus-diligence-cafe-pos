class AddCafesessionAndOrderItemReferencesToOrderLines < ActiveRecord::Migration[5.1]
  def change
    add_reference :order_lines, :cafesession, foreign_key: true
    add_reference :order_lines, :order_item, foreign_key: true
  end
end
