class AddCafesessionAndServiceReferencesToServiceLines < ActiveRecord::Migration[5.2]
  def change
    add_reference :service_lines, :cafesession, foreign_key: true
    add_reference :service_lines, :service, foreign_key: true
  end
end
