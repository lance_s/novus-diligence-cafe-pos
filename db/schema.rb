# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_05_16_133927) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "cafesessions", force: :cascade do |t|
    t.datetime "time_in", null: false
    t.datetime "time_out", null: false
    t.money "subtotal", scale: 2, null: false
    t.date "date", null: false
    t.integer "table_number", limit: 2, null: false
    t.string "discount_code_id", limit: 20, null: false
    t.bigint "card_number"
    t.money "discount_amount", scale: 2, null: false
    t.money "total_amount", scale: 2, null: false
    t.money "vat", scale: 2, null: false
    t.bigint "cashier_id", null: false
    t.bigint "customer_id", null: false
    t.money "cash", scale: 2, null: false
    t.index ["cashier_id"], name: "index_cafesessions_on_cashier_id"
    t.index ["customer_id"], name: "index_cafesessions_on_customer_id"
  end

  create_table "cashiers", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name", limit: 40, null: false
    t.string "address", limit: 100, null: false
    t.string "mobile_number", limit: 12, null: false
    t.date "date_hired", null: false
    t.bigint "admin_id", null: false
    t.index ["admin_id"], name: "index_cashiers_on_admin_id"
    t.index ["email"], name: "index_cashiers_on_email", unique: true
    t.index ["reset_password_token"], name: "index_cashiers_on_reset_password_token", unique: true
  end

  create_table "customers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "customer_name", limit: 40, null: false
    t.string "school_organization", limit: 40, null: false
    t.integer "age", limit: 2, null: false
    t.string "mobile_number", limit: 12, null: false
    t.boolean "membership", null: false
    t.boolean "in_store", null: false
  end

  create_table "discount_codes", id: :string, limit: 20, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "percentage", limit: 2, null: false
    t.boolean "code_status", null: false
  end

  create_table "order_items", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "item_name", limit: 40, null: false
    t.string "item_type", limit: 20, null: false
    t.money "item_price", scale: 2, null: false
    t.integer "item_product_level", limit: 2, null: false
    t.boolean "item_status", null: false
  end

  create_table "order_lines", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "order_quantity", limit: 2, null: false
    t.boolean "order_status", null: false
    t.bigint "cafesession_id", null: false
    t.bigint "order_item_id", null: false
    t.index ["cafesession_id"], name: "index_order_lines_on_cafesession_id"
    t.index ["order_item_id"], name: "index_order_lines_on_order_item_id"
  end

  create_table "service_lines", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "service_quantity", limit: 2, null: false
    t.bigint "cafesession_id"
    t.bigint "service_id"
    t.index ["cafesession_id"], name: "index_service_lines_on_cafesession_id"
    t.index ["service_id"], name: "index_service_lines_on_service_id"
  end

  create_table "services", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "service_name", limit: 25, null: false
    t.money "service_price", scale: 2, null: false
    t.boolean "service_status", null: false
  end

  add_foreign_key "cafesessions", "cashiers"
  add_foreign_key "cafesessions", "customers"
  add_foreign_key "cafesessions", "discount_codes", name: "fk_discount"
  add_foreign_key "cashiers", "admins"
  add_foreign_key "order_lines", "cafesessions"
  add_foreign_key "order_lines", "order_items"
  add_foreign_key "service_lines", "cafesessions"
  add_foreign_key "service_lines", "services"
end
