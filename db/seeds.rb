# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
if Admin.count == 0
    Admin.create!([{email: 'admin@example.com', password: '12341234'}])
end
if Cashier.count == 0
    Cashier.create!([{email: 'cashier@example.com', password: '12341234', name: 'Cashier', admin_id:'1', address: 'Metro Manila', mobile_number: '09171234567', date_hired: Date.today}])
end
if DiscountCode.where("id = NoDiscount") == nil
    DiscountCode.create!([{id: 'NoDiscount', percentage: 0, code_status: true}])
end