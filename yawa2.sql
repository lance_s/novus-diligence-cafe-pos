--
-- PostgreSQL database dump
--

-- Dumped from database version 10.0
-- Dumped by pg_dump version 10.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admins; Type: TABLE; Schema: public; Owner: lancesibi
--

CREATE TABLE admins (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE admins OWNER TO lancesibi;

--
-- Name: admins_id_seq; Type: SEQUENCE; Schema: public; Owner: lancesibi
--

CREATE SEQUENCE admins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admins_id_seq OWNER TO lancesibi;

--
-- Name: admins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lancesibi
--

ALTER SEQUENCE admins_id_seq OWNED BY admins.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: lancesibi
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE ar_internal_metadata OWNER TO lancesibi;

--
-- Name: cafesessions; Type: TABLE; Schema: public; Owner: lancesibi
--

CREATE TABLE cafesessions (
    cafesession_id bigint NOT NULL,
    time_in timestamp without time zone NOT NULL,
    time_out timestamp without time zone NOT NULL,
    subtotal money,
    date date NOT NULL,
    table_number smallint,
    discount_code_id character varying(20),
    card_number bigint NOT NULL,
    discount_amount money NOT NULL,
    total_amount money NOT NULL,
    vat money NOT NULL,
    cashier_id bigint,
    customer_id bigint
);


ALTER TABLE cafesessions OWNER TO lancesibi;

--
-- Name: cafesessions_id_seq; Type: SEQUENCE; Schema: public; Owner: lancesibi
--

CREATE SEQUENCE cafesessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cafesessions_id_seq OWNER TO lancesibi;

--
-- Name: cafesessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lancesibi
--

ALTER SEQUENCE cafesessions_id_seq OWNED BY cafesessions.cafesession_id;


--
-- Name: cashiers; Type: TABLE; Schema: public; Owner: lancesibi
--

CREATE TABLE cashiers (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name character varying(40) NOT NULL,
    address character varying(100) NOT NULL,
    mobile_number character varying(12) NOT NULL,
    date_hired date NOT NULL,
    admin_id bigint NOT NULL
);


ALTER TABLE cashiers OWNER TO lancesibi;

--
-- Name: cashiers_id_seq; Type: SEQUENCE; Schema: public; Owner: lancesibi
--

CREATE SEQUENCE cashiers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cashiers_id_seq OWNER TO lancesibi;

--
-- Name: cashiers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lancesibi
--

ALTER SEQUENCE cashiers_id_seq OWNED BY cashiers.id;


--
-- Name: customers; Type: TABLE; Schema: public; Owner: lancesibi
--

CREATE TABLE customers (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    customer_name character varying(40) NOT NULL,
    school_organization character varying(40) NOT NULL,
    age smallint NOT NULL,
    mobile_number character varying(12) NOT NULL,
    membership boolean NOT NULL,
    in_store boolean NOT NULL
);


ALTER TABLE customers OWNER TO lancesibi;

--
-- Name: customers_id_seq; Type: SEQUENCE; Schema: public; Owner: lancesibi
--

CREATE SEQUENCE customers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customers_id_seq OWNER TO lancesibi;

--
-- Name: customers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lancesibi
--

ALTER SEQUENCE customers_id_seq OWNED BY customers.id;


--
-- Name: discount_codes; Type: TABLE; Schema: public; Owner: lancesibi
--

CREATE TABLE discount_codes (
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    id character varying(20) NOT NULL,
    percentage smallint NOT NULL,
    code_status boolean NOT NULL
);


ALTER TABLE discount_codes OWNER TO lancesibi;

--
-- Name: order_items; Type: TABLE; Schema: public; Owner: lancesibi
--

CREATE TABLE order_items (
    item_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    item_name character varying(40) NOT NULL,
    item_type character varying(20) NOT NULL,
    item_price money NOT NULL,
    item_product_level smallint NOT NULL,
    item_status boolean NOT NULL
);


ALTER TABLE order_items OWNER TO lancesibi;

--
-- Name: order_items_id_seq; Type: SEQUENCE; Schema: public; Owner: lancesibi
--

CREATE SEQUENCE order_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_items_id_seq OWNER TO lancesibi;

--
-- Name: order_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lancesibi
--

ALTER SEQUENCE order_items_id_seq OWNED BY order_items.item_id;


--
-- Name: order_lines; Type: TABLE; Schema: public; Owner: lancesibi
--

CREATE TABLE order_lines (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE order_lines OWNER TO lancesibi;

--
-- Name: order_lines_id_seq; Type: SEQUENCE; Schema: public; Owner: lancesibi
--

CREATE SEQUENCE order_lines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_lines_id_seq OWNER TO lancesibi;

--
-- Name: order_lines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lancesibi
--

ALTER SEQUENCE order_lines_id_seq OWNED BY order_lines.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: lancesibi
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE schema_migrations OWNER TO lancesibi;

--
-- Name: service_lines; Type: TABLE; Schema: public; Owner: lancesibi
--

CREATE TABLE service_lines (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE service_lines OWNER TO lancesibi;

--
-- Name: service_lines_id_seq; Type: SEQUENCE; Schema: public; Owner: lancesibi
--

CREATE SEQUENCE service_lines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE service_lines_id_seq OWNER TO lancesibi;

--
-- Name: service_lines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lancesibi
--

ALTER SEQUENCE service_lines_id_seq OWNED BY service_lines.id;


--
-- Name: services; Type: TABLE; Schema: public; Owner: lancesibi
--

CREATE TABLE services (
    service_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    service_name character varying(25) NOT NULL,
    service_price money NOT NULL,
    service_status boolean NOT NULL
);


ALTER TABLE services OWNER TO lancesibi;

--
-- Name: services_id_seq; Type: SEQUENCE; Schema: public; Owner: lancesibi
--

CREATE SEQUENCE services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE services_id_seq OWNER TO lancesibi;

--
-- Name: services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lancesibi
--

ALTER SEQUENCE services_id_seq OWNED BY services.service_id;


--
-- Name: admins id; Type: DEFAULT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY admins ALTER COLUMN id SET DEFAULT nextval('admins_id_seq'::regclass);


--
-- Name: cafesessions cafesession_id; Type: DEFAULT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY cafesessions ALTER COLUMN cafesession_id SET DEFAULT nextval('cafesessions_id_seq'::regclass);


--
-- Name: cashiers id; Type: DEFAULT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY cashiers ALTER COLUMN id SET DEFAULT nextval('cashiers_id_seq'::regclass);


--
-- Name: customers id; Type: DEFAULT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY customers ALTER COLUMN id SET DEFAULT nextval('customers_id_seq'::regclass);


--
-- Name: order_items item_id; Type: DEFAULT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY order_items ALTER COLUMN item_id SET DEFAULT nextval('order_items_id_seq'::regclass);


--
-- Name: order_lines id; Type: DEFAULT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY order_lines ALTER COLUMN id SET DEFAULT nextval('order_lines_id_seq'::regclass);


--
-- Name: service_lines id; Type: DEFAULT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY service_lines ALTER COLUMN id SET DEFAULT nextval('service_lines_id_seq'::regclass);


--
-- Name: services service_id; Type: DEFAULT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY services ALTER COLUMN service_id SET DEFAULT nextval('services_id_seq'::regclass);


--
-- Data for Name: admins; Type: TABLE DATA; Schema: public; Owner: lancesibi
--

COPY admins (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at) FROM stdin;
1	2@2.com	$2a$11$m0UkVZotwMot9H0JZ9iph.Fj3wFZdPAwWfW4DHcok874rSZ.xziR.	\N	\N	\N	11	2018-05-10 10:52:18.64596	2018-05-10 09:14:32.001085	127.0.0.1	127.0.0.1	2018-05-03 06:14:37.476475	2018-05-10 10:52:18.647135
\.


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: lancesibi
--

COPY ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2018-04-10 10:46:21.902115	2018-04-10 10:46:21.902115
\.


--
-- Data for Name: cafesessions; Type: TABLE DATA; Schema: public; Owner: lancesibi
--

COPY cafesessions (cafesession_id, time_in, time_out, subtotal, date, table_number, discount_code_id, card_number, discount_amount, total_amount, vat, cashier_id, customer_id) FROM stdin;
14	2018-05-10 10:38:58.603169	2018-05-10 10:38:58.603516	$0.00	2018-05-10	1	NoDiscount	0	$0.00	$0.00	$0.00	19	3
15	2018-05-10 10:41:02.368449	2018-05-10 10:41:02.368691	$0.00	2018-05-10	1	NoDiscount	0	$0.00	$0.00	$0.00	19	4
16	2018-05-10 10:41:05.011501	2018-05-10 10:41:05.011526	$0.00	2018-05-10	1	NoDiscount	0	$0.00	$0.00	$0.00	19	12
17	2018-05-10 10:41:12.212303	2018-05-10 10:41:12.212328	$0.00	2018-05-10	1	NoDiscount	0	$0.00	$0.00	$0.00	19	13
18	2018-05-10 10:41:15.684897	2018-05-10 10:41:15.684917	$0.00	2018-05-10	1	NoDiscount	0	$0.00	$0.00	$0.00	19	5
19	2018-05-10 10:41:17.531318	2018-05-10 10:41:17.531337	$0.00	2018-05-10	1	NoDiscount	0	$0.00	$0.00	$0.00	19	14
\.


--
-- Data for Name: cashiers; Type: TABLE DATA; Schema: public; Owner: lancesibi
--

COPY cashiers (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at, name, address, mobile_number, date_hired, admin_id) FROM stdin;
19	1@1.com	$2a$11$qZK7HK1HH0G/Z.Cekj/I0OuRMnPj1Giajd7GjCdxOwMvJKWShhOYu	\N	\N	\N	5	2018-05-10 11:07:12.523738	2018-05-10 09:22:54.088881	127.0.0.1	127.0.0.1	2018-05-09 17:41:41.63166	2018-05-10 11:07:12.534721	OSSAS	nigeria OSSAs	420420420	2018-04-20	1
\.


--
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: lancesibi
--

COPY customers (id, created_at, updated_at, customer_name, school_organization, age, mobile_number, membership, in_store) FROM stdin;
3	2018-04-13 13:24:10.433563	2018-05-10 10:38:58.646324	Lol	Why	89	55	f	t
4	2018-04-13 13:51:57.859223	2018-05-10 10:41:02.382561	yawa	yawa org	389	55	f	t
12	2018-04-19 07:28:03.503144	2018-05-10 10:41:05.023103	bhfgjh	jkhkh	99	55	f	t
13	2018-04-27 09:31:20.701221	2018-05-10 10:41:12.218331	kkkkjkk	jkkjkkjkjk	35	55	f	t
5	2018-04-14 17:25:48.168794	2018-05-10 10:41:15.696966	hello	haha org	88	55	f	t
14	2018-05-03 06:20:53.305214	2018-05-10 10:41:17.546079	dfgsgs	dsfgsdfgs	21	2345	f	t
\.


--
-- Data for Name: discount_codes; Type: TABLE DATA; Schema: public; Owner: lancesibi
--

COPY discount_codes (created_at, updated_at, id, percentage, code_status) FROM stdin;
2018-05-09 14:22:04.353679	2018-05-09 14:22:04.353679	yawaa	21	f
2018-05-09 13:27:53.197597	2018-05-09 18:28:51.686765	yawa	21	t
2018-04-15 02:58:00.977709	2018-05-09 18:31:56.033872	yawaaaa	21	t
2018-04-15 10:34:33.715165	2018-05-09 18:55:30.474639	ayoko na	21	t
2018-04-19 07:27:36.915094	2018-05-09 18:55:40.084343	pisti na	21	t
2018-04-15 02:58:07.602464	2018-05-09 18:55:45.694212	uli na	21	f
2018-05-10 08:04:30.134684	2018-05-10 08:04:30.134684	NoDiscount	0	t
\.


--
-- Data for Name: order_items; Type: TABLE DATA; Schema: public; Owner: lancesibi
--

COPY order_items (item_id, created_at, updated_at, item_name, item_type, item_price, item_product_level, item_status) FROM stdin;
2	2018-04-16 13:50:00.131718	2018-04-16 13:50:00.131718	{fasda}	{Food}	$234.00	23	f
3	2018-04-16 13:56:28.263075	2018-04-16 13:56:28.263075	{dsfdf}	{Food}	$0.32	23	t
4	2018-04-16 13:56:46.86563	2018-04-16 13:56:46.86563	{234}	{Food}	$6.00	2	t
12	2018-04-16 14:47:32.543251	2018-04-16 14:47:32.543251	{}	{Food}	$3.45	-9	t
13	2018-04-16 14:48:40.316237	2018-04-16 14:48:40.316237	{gfsffs}	{Food}	$43.00	234	f
14	2018-04-17 01:36:19.620267	2018-04-17 01:36:19.620267	{"French Fries"}	{Drink}	$35.00	35	f
15	2018-04-19 07:26:57.549255	2018-04-19 07:26:57.549255	{hjgjhghj}	{Food}	$56,786.00	20	t
16	2018-05-07 16:14:12.038514	2018-05-07 16:14:12.038514	Oplok	{Food}	$234.00	23	f
17	2018-05-09 14:32:29.264608	2018-05-09 14:32:29.264608	oplok	{Food}	$234.00	234	f
1	2018-04-16 13:49:31.235904	2018-05-09 18:28:05.868454	MoO	{Food}	$99.99	234	t
\.


--
-- Data for Name: order_lines; Type: TABLE DATA; Schema: public; Owner: lancesibi
--

COPY order_lines (id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: lancesibi
--

COPY schema_migrations (version) FROM stdin;
20180410104217
20180410112529
20180410112541
20180410112728
20180410122726
20180410122737
20180410123043
20180417064607
20180417065333
20180419123925
20180424074427
20180424080934
20180424080940
20180427134010
20180427134944
20180427135220
\.


--
-- Data for Name: service_lines; Type: TABLE DATA; Schema: public; Owner: lancesibi
--

COPY service_lines (id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: services; Type: TABLE DATA; Schema: public; Owner: lancesibi
--

COPY services (service_id, created_at, updated_at, service_name, service_price, service_status) FROM stdin;
1	2018-04-17 01:45:23.949343	2018-04-17 01:45:23.949343	{Shower}	$35.00	f
2	2018-04-17 01:45:32.853504	2018-04-17 01:45:32.853504	{Locker}	$100.00	t
\.


--
-- Name: admins_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lancesibi
--

SELECT pg_catalog.setval('admins_id_seq', 1, true);


--
-- Name: cafesessions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lancesibi
--

SELECT pg_catalog.setval('cafesessions_id_seq', 19, true);


--
-- Name: cashiers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lancesibi
--

SELECT pg_catalog.setval('cashiers_id_seq', 19, true);


--
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lancesibi
--

SELECT pg_catalog.setval('customers_id_seq', 15, true);


--
-- Name: order_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lancesibi
--

SELECT pg_catalog.setval('order_items_id_seq', 17, true);


--
-- Name: order_lines_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lancesibi
--

SELECT pg_catalog.setval('order_lines_id_seq', 1, false);


--
-- Name: service_lines_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lancesibi
--

SELECT pg_catalog.setval('service_lines_id_seq', 1, false);


--
-- Name: services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lancesibi
--

SELECT pg_catalog.setval('services_id_seq', 2, true);


--
-- Name: admins admins_pkey; Type: CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY admins
    ADD CONSTRAINT admins_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: cafesessions cafesessions_pkey; Type: CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY cafesessions
    ADD CONSTRAINT cafesessions_pkey PRIMARY KEY (cafesession_id);


--
-- Name: cashiers cashiers_pkey; Type: CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY cashiers
    ADD CONSTRAINT cashiers_pkey PRIMARY KEY (id);


--
-- Name: customers customers_pkey; Type: CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);


--
-- Name: discount_codes discount_code_pkey; Type: CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY discount_codes
    ADD CONSTRAINT discount_code_pkey PRIMARY KEY (id);


--
-- Name: order_items order_items_pkey; Type: CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY order_items
    ADD CONSTRAINT order_items_pkey PRIMARY KEY (item_id);


--
-- Name: order_lines order_lines_pkey; Type: CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY order_lines
    ADD CONSTRAINT order_lines_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: service_lines service_lines_pkey; Type: CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY service_lines
    ADD CONSTRAINT service_lines_pkey PRIMARY KEY (id);


--
-- Name: services services_pkey; Type: CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY services
    ADD CONSTRAINT services_pkey PRIMARY KEY (service_id);


--
-- Name: index_admins_on_email; Type: INDEX; Schema: public; Owner: lancesibi
--

CREATE UNIQUE INDEX index_admins_on_email ON admins USING btree (email);


--
-- Name: index_admins_on_reset_password_token; Type: INDEX; Schema: public; Owner: lancesibi
--

CREATE UNIQUE INDEX index_admins_on_reset_password_token ON admins USING btree (reset_password_token);


--
-- Name: index_cafesessions_on_cashier_id; Type: INDEX; Schema: public; Owner: lancesibi
--

CREATE INDEX index_cafesessions_on_cashier_id ON cafesessions USING btree (cashier_id);


--
-- Name: index_cafesessions_on_customer_id; Type: INDEX; Schema: public; Owner: lancesibi
--

CREATE INDEX index_cafesessions_on_customer_id ON cafesessions USING btree (customer_id);


--
-- Name: index_cashiers_on_admin_id; Type: INDEX; Schema: public; Owner: lancesibi
--

CREATE INDEX index_cashiers_on_admin_id ON cashiers USING btree (admin_id);


--
-- Name: index_cashiers_on_email; Type: INDEX; Schema: public; Owner: lancesibi
--

CREATE UNIQUE INDEX index_cashiers_on_email ON cashiers USING btree (email);


--
-- Name: index_cashiers_on_reset_password_token; Type: INDEX; Schema: public; Owner: lancesibi
--

CREATE UNIQUE INDEX index_cashiers_on_reset_password_token ON cashiers USING btree (reset_password_token);


--
-- Name: cafesessions fk_discount; Type: FK CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY cafesessions
    ADD CONSTRAINT fk_discount FOREIGN KEY (discount_code_id) REFERENCES discount_codes(id);


--
-- Name: cafesessions fk_rails_658e56ba46; Type: FK CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY cafesessions
    ADD CONSTRAINT fk_rails_658e56ba46 FOREIGN KEY (customer_id) REFERENCES customers(id);


--
-- Name: cashiers fk_rails_ab97e5b4fe; Type: FK CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY cashiers
    ADD CONSTRAINT fk_rails_ab97e5b4fe FOREIGN KEY (admin_id) REFERENCES admins(id);


--
-- Name: cafesessions fk_rails_c48f9db47c; Type: FK CONSTRAINT; Schema: public; Owner: lancesibi
--

ALTER TABLE ONLY cafesessions
    ADD CONSTRAINT fk_rails_c48f9db47c FOREIGN KEY (cashier_id) REFERENCES cashiers(id);


--
-- PostgreSQL database dump complete
--

